from . import hashing, redis_sender
from models import account as models
from models import journal as journal_model
from datastore import account as datastore
from datastore.datastore import openTx
from log_config import account_logger as log
from fastapi import HTTPException
import json

def daftar(request: models.DaftarRequest) -> models.DaftarResponse:
    try:
        session = openTx()
        account_check = models.Account(**request.model_dump())
        account = datastore.getAccount(session, account_check)
        if account.nik == request.nik:
            raise HTTPException(400, "NIK sudah digunakan")
        
        if account.no_hp == request.no_hp:
            raise HTTPException(400, "Nomor HP sudah digunakan")
        
        request.pin = hashing.hashpass(request.pin)
        no_rek = datastore.daftar(session, request)
        response = models.DaftarResponse(message="Success", no_rekening=no_rek)

        session.commit()
        session.close()
        log.info(f"Daftar sukses dengan norek: {no_rek}")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

def deposit(request: models.DepositRequest) -> models.DepositResponse:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request.no_rekening)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")

        transaction = models.TransactionRequest(no_rekening=request.no_rekening, nominal=request.nominal, kode_transaksi="C")
        transactionResp = datastore.transaction(session, transaction)
        response = models.DepositResponse(saldo=(account.saldo + request.nominal))

        #Redis
        message = journal_model.Journal(
            id_transaksi=transactionResp.id_transaksi,
            tanggal_transaksi=transactionResp.tanggal_transaksi,
            no_rekening_kredit=request.no_rekening,
            nominal_kredit=request.nominal
        )
        json_message = json.dumps(message.model_dump(), indent=2)
        redis_sender.sendMessage(channel="journal", message=json_message)
        #--

        session.commit()
        session.close()
        log.info(f"Deposit sukses dengan id transaksi: {transactionResp.id_transaksi}")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

def withdraw(request: models.WithdrawRequest) -> models.WithdrawResponse:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request.no_rekening)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")
        
        if account.saldo < request.nominal:
            raise HTTPException(400, "Saldo tidak cukup")

        transaction = models.TransactionRequest(no_rekening=request.no_rekening, nominal=request.nominal, kode_transaksi="D")
        transactionResp = datastore.transaction(session, transaction)
        response = models.WithdrawResponse(saldo=(account.saldo - request.nominal))

        #Redis
        message = journal_model.Journal(
            id_transaksi=transactionResp.id_transaksi,
            tanggal_transaksi=transactionResp.tanggal_transaksi,
            no_rekening_debit=request.no_rekening,
            nominal_debit=request.nominal
        )
        json_message = json.dumps(message.model_dump(), indent=2)
        redis_sender.sendMessage(channel="journal", message=json_message)
        #--
        
        session.commit()
        session.close()
        log.info(f"Withdraw sukses dengan id transaksi: {transactionResp.id_transaksi}")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

def transfer(request: models.TransferRequest) -> models.TransferResponse:
    try:
        session = openTx()

        #Cek akun asal & tujuan
        origin_account_check = models.Account(no_rekening=request.no_rekening_asal)
        origin_account = datastore.getAccount(session, origin_account_check)
        if origin_account.no_rekening == "":
            raise HTTPException(400, "Rekening asal tidak ditemukan")
        
        if origin_account.saldo < request.nominal:
            raise HTTPException(400, "Saldo tidak cukup")
        
        destination_account_check = models.Account(no_rekening=request.no_rekening_tujuan)
        destination_account = datastore.getAccount(session, destination_account_check)
        if destination_account.no_rekening == "":
            raise HTTPException(400, "Rekening tujuan tidak ditemukan")
        
        #Transaksi debet & kredit
        transaction = models.TransactionRequest(no_rekening=request.no_rekening_asal, no_rekening_tujuan=request.no_rekening_tujuan ,nominal=request.nominal, kode_transaksi="T")
        transactionResp = datastore.transaction(session, transaction)
        response = models.TransferResponse(saldo=(origin_account.saldo - request.nominal))

        #Redis
        message = journal_model.Journal(
            id_transaksi=transactionResp.id_transaksi,
            tanggal_transaksi=transactionResp.tanggal_transaksi,
            no_rekening_debit=request.no_rekening_asal,
            nominal_debit=request.nominal,
            no_rekening_kredit=request.no_rekening_tujuan,
            nominal_kredit=request.nominal
        )
        json_message = json.dumps(message.model_dump(), indent=2)
        redis_sender.sendMessage(channel="journal", message=json_message)
        #--
        
        session.commit()
        session.close()
        log.info(f"Transfer sukses dengan id transaksi: {transactionResp.id_transaksi}")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e

def getBalance(request: str) -> float:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")
        
        session.commit()
        session.close()
        log.info(f"Getsaldo sukses dengan norek: {account.no_rekening}")
        return account.saldo
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e
    
def getTransactions(request: str) -> models.MutationResponse:
    try:
        session = openTx()
        account_check = models.Account(no_rekening=request)
        account = datastore.getAccount(session, account_check)
        if account.no_rekening == "":
            raise HTTPException(400, "Rekening tidak ditemukan")
        
        transactions = datastore.getTransactions(session, request)
        response = models.MutationResponse(
            mutasi = transactions
        )
        
        session.commit()
        session.close()
        log.info(f"Mutasi sukses dengan norek: {account.no_rekening}")
        return response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"App error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e