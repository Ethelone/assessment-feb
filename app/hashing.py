from bcrypt import hashpw, gensalt, checkpw
from log_config import auth_logger as log

def hashpass(password_in: str) -> str:
    try:
        hashed_pass = hashpw(password_in.encode('utf-8'), gensalt())
        return hashed_pass.decode('utf-8')
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Failed to hashpass \n error: {e} \n filename: {filename} - line: {line_number}")
        raise e
    
def checkPin(input_pin: str, hashed_pin:str) -> bool:
    try:
        return checkpw(input_pin.encode('utf-8'), hashed_pin.encode('utf-8'))
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Failed to checkPin \n error: {e} \n filename: {filename} - line: {line_number}")
        raise e