from sqlalchemy import  MetaData, Table, Column, String, Sequence, func, text, cast, Float, TIMESTAMP

metadata = MetaData()

account_number_seq = Sequence('account_number_seq', metadata=metadata)

def account_number_func(value, total_digits=8):
    return func.lpad(cast(value, String), total_digits, '0')

trx_seq = Sequence('trx_seq', metadata=metadata)

def trx_func(value, total_digits=16):
    return func.lpad(cast(value, String), total_digits, '0')

journal_id_seq = Sequence('journal_id_seq', metadata=metadata)

def journal_id_func(value, total_digits=16):
    return func.lpad(cast(value, String), total_digits, '0')
    
auth = Table(
    "auth",
    metadata,
    Column("no_rekening", String, primary_key=True, server_default=account_number_func(func.nextval('account_number_seq'))),
    Column("pin", String),
)

account = Table(
    "account",
    metadata,
    Column("no_rekening", String, primary_key=True),
    Column("nama", String),
    Column("nik", String),
    Column("no_hp", String),
    Column("saldo", Float, default=0.0),
)

transaction = Table(
    "transaction",
    metadata,
    Column("id_transaksi", String, primary_key=True, server_default=trx_func(func.nextval('trx_seq'))),
    Column("no_rekening", String, primary_key=True),
    Column("waktu", TIMESTAMP),
    Column("kode_transaksi", String),
    Column("nominal", Float, default=0.0),
    Column("keterangan", String, default="")
)

journal = Table(
    "journal",
    metadata,
    Column("id_journal", String, primary_key=True, server_default=journal_id_func(func.nextval('journal_id_seq'))),
    Column("id_transaksi", String),
    Column("tanggal_transaksi", TIMESTAMP),
    Column("no_rekening_kredit", String, default=""),
    Column("no_rekening_debit", String, default=""),
    Column("nominal_kredit", Float, default=0.0),
    Column("nominal_debit", Float, default=0.0),

)