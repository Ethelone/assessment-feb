from sqlalchemy import select, insert, or_, update, func, String
from models import account as models
from log_config import account_logger as log
from fastapi import HTTPException
import datastore.tables as tables
from sqlalchemy.orm import Session
from typing import List

def daftar(session: Session, request: models.DaftarRequest) -> str:
    log.info(f"Daftar datastore for NIK {request.nik}")
    try:
        #Check for NIK dupe
        account_table = tables.account
        auth_table = tables.auth

        statement = insert(auth_table).values(
            pin = request.pin
            ).returning(auth_table.c.no_rekening)
        no_rekening = session.execute(statement).fetchone()[0]

        statement = insert(account_table).values(
            no_rekening = no_rekening,
            nama = request.nama,
            nik = request.nik,
            no_hp = request.no_hp
            )
        session.execute(statement)
        return no_rekening

    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e
        
def getAccount(session: Session, request: models.Account) -> models.Account:
    log.info(f"Get Account with payload {request}")
    
    try:
        account_table = tables.account
        condition = or_(
            account_table.c.no_rekening == request.no_rekening,
            account_table.c.nik == request.nik,
            account_table.c.no_hp == request.no_hp
        )
        statement = select(account_table).where(condition)
        dbResponse = session.execute(statement).fetchone()
        result = models.Account()
        if dbResponse is None:
            return result
        dbResponse = dbResponse._asdict()
        result = models.Account(**dbResponse)   
        return result

    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e
        
def transaction(session: Session, request: models.TransactionRequest) -> models.TransactionResponse:
    log.info(f"Transaction from acct no: {request.no_rekening}")
    try:
        keterangan = ""
        if request.kode_transaksi == "T": #Handle transfer
            keterangan = f"Transfer dari: {request.no_rekening} tujuan: {request.no_rekening_tujuan} nominal: {request.nominal}"
            
            #Create trx tarik
            transaction_table = tables.transaction
            statement = insert(transaction_table).values(
                no_rekening = request.no_rekening,
                waktu = func.now(),
                kode_transaksi = request.kode_transaksi,
                nominal = request.nominal,
                keterangan = keterangan
                ).returning(
                    transaction_table.c.id_transaksi,
                    func.cast(transaction_table.c.waktu, String)
                )
            result = session.execute(statement).fetchone()
            id_transaksi, waktu = result[0], result[1]

            #Create trx setor
            transaction_table = tables.transaction
            statement = insert(transaction_table).values(
                id_transaksi = id_transaksi,
                no_rekening = request.no_rekening_tujuan,
                waktu = func.now(),
                kode_transaksi = request.kode_transaksi,
                nominal = request.nominal,
                keterangan = keterangan
                )
            session.execute(statement)

        else: #Handle tarik/setor
            keterangan = f"Setor tunai acct_no: {request.no_rekening} nominal: {request.nominal}"
            if request.kode_transaksi == "D":
                keterangan = f"Tarik tunai acct_no: {request.no_rekening} nominal: {request.nominal}"
                
            #Create trx
            transaction_table = tables.transaction
            statement = insert(transaction_table).values(
                no_rekening = request.no_rekening,
                waktu = func.now(),
                kode_transaksi = request.kode_transaksi,
                nominal = request.nominal,
                keterangan = keterangan
                ).returning(
                    transaction_table.c.id_transaksi,
                    func.cast(transaction_table.c.waktu, String)
                )
            result = session.execute(statement).fetchone()
            id_transaksi, waktu = result[0], result[1]

        response = models.TransactionResponse(id_transaksi=id_transaksi, tanggal_transaksi=waktu)
        session.commit()
        session.close()
        return response

    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e
    
def getTransactions(session: Session, request: str) -> List[models.Transaction]:
    log.info(f"Get Transactions from acct {request}")
    
    try:
        transaction_table = tables.transaction
        statement = (
            select(
                transaction_table.c.id_transaksi,
                transaction_table.c.waktu,
                transaction_table.c.kode_transaksi,
                transaction_table.c.nominal,
                transaction_table.c.keterangan
            )
            .where(transaction_table.c.no_rekening == request)
            )
        dbResponse = session.execute(statement).fetchall()
        if not dbResponse:
            return []
        
        result = [models.Transaction(
            id_transaksi=row[0],
            waktu=str(row[1]),
            kode_transaksi=row[2],
            nominal=row[3],
            keterangan=row[4]
        ) for row in dbResponse]
        return result

    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e