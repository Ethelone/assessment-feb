from .datastore import openTx
from sqlalchemy import select
from models import auth as models
from log_config import auth_logger as log
from fastapi import HTTPException
import datastore.tables as tables

def cekPin(session, request: models.CekpinRequest) -> models.CekpinResponse:
    log.info(f"Cek Pin datastore for acct {request.no_rekening}")
    try:
        auth_table = tables.auth
        statement = select(auth_table).where(auth_table.c.no_rekening == request.no_rekening)
        result = session.execute(statement).fetchone()
        if result is None:
            raise HTTPException(400, "Nomor Rekening tidak ditemukan")
        
        datastore_response = models.CekpinResponse(
            no_rekening=result.no_rekening,
            pin=result.pin
        )

        return datastore_response
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e