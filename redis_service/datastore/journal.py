from sqlalchemy import insert, update, func
from models import journal as models
from log_config import journal_logger as log
import datastore.tables as tables
from sqlalchemy.orm import Session

def createJournal(session: Session, request: models.Journal)-> str:
    log.info(f"Create journal for id_trx {request.id_transaksi}")
    try:
        #Create trx tarik
        journal_table = tables.journal
        statement = insert(journal_table).values(
            id_transaksi = request.id_transaksi,
            tanggal_transaksi = func.to_timestamp(request.tanggal_transaksi, 'YYYY-MM-DDTHH24:MI:SS.US'),
            no_rekening_kredit = request.no_rekening_kredit,
            no_rekening_debit = request.no_rekening_debit,
            nominal_kredit = request.nominal_kredit,
            nominal_debit = request.nominal_debit
        ).returning(
            journal_table.c.id_journal
        )
        id_journal = session.execute(statement).fetchone()[0]
        return id_journal
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e
    
def processJournal(session: Session, no_rekening: str, nominal: float):
    log.info(f"Process journal for acct_no {no_rekening}")
    try:
        account_table = tables.account
        statement = (
            update(account_table)
            .where(account_table.c.no_rekening == no_rekening)
            .values(
                saldo=account_table.c.saldo + nominal)
        )
        session.execute(statement)
        return
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Datastore error: {e} \n filename: {filename} - line: {line_number}")
        raise e

       