import redis
from redis_service.datastore import journal as datastore
from datastore.datastore import openTx
from models import journal as models
from log_config import journal_logger as log
from config import Config
import json

async def journal_subscribe():
    try:
        redis_client = redis.StrictRedis(host=Config.REDIS_HOST, port=Config.REDIS_PORT, decode_responses=True)
        pubsub = redis_client.pubsub()
        pubsub.subscribe('journal')
        
        for message in pubsub.listen():
            if message['type'] == 'message':
                await process_journal(message['data'])
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.critical(f"Journal subscribe error: {e} \n filename: {filename} - line: {line_number}")

async def process_journal(message):
    log.info(f"Journal triggered: {message}")
    try:
        session = openTx()
        json_message = json.loads(message)
        journal = models.Journal(**json_message)
        #Create journal
        id_journal = datastore.createJournal(session=session, request=journal)

        #Process journal
        if journal.no_rekening_kredit != "":
            datastore.processJournal(session=session, no_rekening=journal.no_rekening_kredit, nominal=journal.nominal_kredit)

        if journal.no_rekening_debit != "":
            datastore.processJournal(session=session, no_rekening=journal.no_rekening_debit, nominal=-journal.nominal_debit)

        session.commit()
        session.close()
        log.info(f"Journal success with id_journal: {id_journal}")
        return

    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.critical(f"Journal error: {e} \n filename: {filename} - line: {line_number}")
        session.rollback()
        raise e
