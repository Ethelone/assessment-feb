from pydantic import BaseModel
from typing import List

class DaftarRequest(BaseModel):
    nama: str
    nik: str
    no_hp: str
    pin: str

class DaftarResponse(BaseModel):
    message: str
    no_rekening: str

class DepositRequest(BaseModel):
    no_rekening: str
    nominal: float

class DepositResponse(BaseModel):
    saldo: float

class WithdrawRequest(BaseModel):
    no_rekening: str
    nominal: float

class WithdrawResponse(BaseModel):
    saldo: float

class TransferRequest(BaseModel):
    no_rekening_asal: str
    no_rekening_tujuan: str
    nominal: float

class TransferResponse(BaseModel):
    saldo: float

class Account(BaseModel):
    no_rekening: str = ''
    nama: str = ''
    nik: str = ''
    no_hp: str = ''
    saldo: float = 0.0

class TransactionRequest(BaseModel):
    no_rekening: str
    no_rekening_tujuan: str = ""
    nominal: float
    kode_transaksi: str

class TransactionResponse(BaseModel):
    id_transaksi: str
    tanggal_transaksi: str = ""

class Transaction(BaseModel):
    id_transaksi: str
    waktu: str
    kode_transaksi: str
    nominal: float
    keterangan: str

class MutationResponse(BaseModel):
    mutasi: List[Transaction] = []
