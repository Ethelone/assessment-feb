from pydantic import BaseModel

class Journal(BaseModel):
    id_transaksi: str
    tanggal_transaksi: str
    no_rekening_kredit: str =""
    no_rekening_debit: str =""
    nominal_kredit: float = 0.0
    nominal_debit: float = 0.0