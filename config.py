import os
from dotenv import load_dotenv

load_dotenv()

class Config:
    PROJECT_NAME = os.getenv("PROJECT_NAME")
    DEFAULT_URL = os.getenv("DEFAULT_URL")
    PORT =  int(os.getenv("DEFAULT_PORT"))

    LOG_FILE_DIRECTORY = os.getenv("LOG_FILE_DIRECTORY")

    
    DB_URL = os.getenv("POSTGRES_URL")
    DB_PORT = os.getenv("POSTGRES_PORT")
    DB_USER = os.getenv("POSTGRES_USER")
    DB_PW = os.getenv("POSTGRES_PASSWORD")
    DB_MAINT = os.getenv("POSTGRES_DB")

    REDIS_HOST = os.getenv("REDIS_HOST")
    REDIS_PORT = os.getenv("REDIS_PORT")
