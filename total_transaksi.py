from datastore.datastore import openTx
from datastore import tables as tables
from sqlalchemy import select, func, case
import sys

def total_transaksi(tanggal_awal, tanggal_akhir):
    session = openTx()

    tanggal_akhir += "::23:59:59"

    table = tables.transaction
    statement = select(
        func.coalesce(func.count(table.c.id_transaksi), 0).label('total_transaksi'),
        func.coalesce(func.sum(case((table.c.kode_transaksi == 'C', table.c.nominal), else_=0.0)), 0.0).label('jumlah_nominal_setor'),
        func.coalesce(func.sum(case((table.c.kode_transaksi == 'D', table.c.nominal), else_=0.0)), 0.0).label('jumlah_nominal_tarik'),
        func.coalesce(func.sum(case((table.c.kode_transaksi == 'T', table.c.nominal), else_=0.0)), 0.0).label('jumlah_nominal_transfer')
    ).where(
        (table.c.waktu >= tanggal_awal) & (table.c.waktu <= tanggal_akhir)
    )

    result = session.execute(statement).fetchone()
    session.close()
    print(f"Total transaksi: {result[0]} | Jumlah nominal setor: {result[1]} | Jumlah nominal tarik: {result[2]} | Jumlah nominal transfer: {result[3]}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python total_transaksi.py <tanggal_awal> <tanggal_akhir> (in \"YYYY-MM-DD\")")
    else:
        tanggal_awal = sys.argv[1]
        tanggal_akhir = sys.argv[2]
        total_transaksi(tanggal_awal, tanggal_akhir)