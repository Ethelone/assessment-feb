from fastapi import Request, HTTPException, Response
from app import auth as app
from models import auth as models
from log_config import auth_logger as log
import json
from log_config import auth_logger as log

async def authenticate_user(request: Request, call_next):
    try:
        path = request.scope['path']
        if path != "/account/daftar":
            if path.startswith("/account/saldo") or path.startswith("/account/mutasi") :
                _, _, no_rekening = path.rpartition("/")
                pin = request.headers.get('pin')

                log.info(f"Authenticating: {no_rekening}")
                app.cekPin(models.CekpinRequest(no_rekening=no_rekening, pin=pin))
            else:
                body = await request.json()
                if path == "/account/transfer":
                    no_rekening = body["no_rekening_asal"]
                else:
                    no_rekening = body["no_rekening"]
                pin = request.headers.get('pin')

                log.info(f"Authenticating: {no_rekening}")
                app.cekPin(models.CekpinRequest(no_rekening=no_rekening, pin=pin))

        response = await call_next(request)
        return response
    except HTTPException as e:
        log.error(f"HTTP Err: {e}")
        content = {"remark":e.detail}
        content = json.dumps(content)
        return Response(content=content, status_code=e.status_code)
    except Exception as e:
        filename = e.__traceback__.tb_frame.f_code.co_filename
        line_number = e.__traceback__.tb_lineno
        log.error(f"Auth error: {e} \n filename: {filename} - line: {line_number}")
        raise e
