from fastapi import APIRouter
from api import account

router = APIRouter()

router.include_router(account.router, prefix="/account")

# #Default
# @router.get("/")
# def getdefault():
#     return{"message":"Connected"}